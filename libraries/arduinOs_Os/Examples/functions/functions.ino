#include <Command.h>

Action root("main");

///////////////////////////////////////////////////////
void test() {
///////////////////////////////////////////////////////
    //destruction auto a la fin de l'execution de la fonction
    //si pointeurs Action* var=new Action();
    //delete var; pour detruire
    // attention la mémoire explose vite

    Serial.println("ok");
    TestAction a(&root,"a");
    TestAction b(&a,"b");
    a.setup();
    a.call();
    root.tree();

}
/////////////////////////////////////////////////////////////////
Execute command(&root,"cmd",test);
Query query(&root,"query","/cmd");


///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////

    Serial.begin(9600);

    title("setup");
    root.setup();

    title("call"); 
    command.call(); 
    root.tree();
    title("query"); 
    query.call(); 
    root.tree();
    line();
}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////


}
///////////////////////////////////////////////////////
