#include "Module.h"


 
//----------------------------------------------------------------
Module::Module(TreeNode* parent,String name) : TreeNode(parent,name){

}
//----------------------------------------------------------------
Module::~Module(){/*nothing to destruct*/}

//----------------------------------------------------------------
void Module::query(String path,String command){
    Serial.println("Module::query ");
    TreeNode* obj=this->find(path);


    if( obj ){
        Serial.println("OBJECT ");
        this->onQuery(obj,command);
    }else{

        Serial.println("ERROR no path ");
    }
}
//----------------------------------------------------------------
void Module::onQuery(TreeNode* obj,String command){


}
//----------------------------------------------------------------
// lifecycle methods
//----------------------------------------------------------------     
/*void Module::setup(void){

    this->log("setup");
    this->onSetup();

    for (unsigned i=0; i < this->children.size(); i++) {
        this->children[i]->setup();
    }
}*/
//----------------------------------------------------------------
void Module::debug(void){

    this->onDebug();

    for (unsigned i=0; i < this->children.size(); i++) {
        //this->children[i]->debug();
    }
}

//----------------------------------------------------------------
void Module::read(void){

    this->log("read");
    this->onRead();

    for (unsigned i=0; i < this->children.size(); i++) {
        //this->children[i]->read();
    }
}
//----------------------------------------------------------------
void Module::exec(void){

    //this->send_message("exec");
    this->onExec();

    for (unsigned i=0; i < this->children.size(); i++) {
        //this->children[i]->exec();
    }
}
//----------------------------------------------------------------
void Module::write(void){

    this->log("write");
    this->onWrite();

    for (unsigned i=0; i < this->children.size(); i++) {
        //this->children[i]->write();
    }
}
//----------------------------------------------------------------
//----------------------------------------------------------------

