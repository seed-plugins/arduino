
#ifndef MY_TIME_H
#define MY_TIME_H

#include <Device.h>

/////////////////////////////////////////////////////////////////
class Time : public Device {
/////////////////////////////////////////////////////////////////


    public:

       Time(TreeNode* parent,String name): Device(parent,name){};
       ~Time(){};

       //----------------------------------------------------------------
       virtual void get_data(JsonObject data){

            if (this->active and this->is_input){
                data[this->node_id]=millis()/1000.0;
            }     
        };
       //----------------------------------------------------------------

};
/////////////////////////////////////////////////////////////////
class Counter : public Device {
/////////////////////////////////////////////////////////////////


    public:
        int counter=0;
       Counter(TreeNode* parent,String name): Device(parent,name){};
       ~Counter(){};

       //----------------------------------------------------------------
       virtual void get_data(JsonObject data){

            if (this->active and this->is_input){
                data[this->node_id]=counter;
                counter+=1;
            }     
        };
       //----------------------------------------------------------------

};
/////////////////////////////////////////////////////////////////




#endif
