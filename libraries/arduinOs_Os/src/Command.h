#ifndef COMMAND_H
#define COMMAND_H

#include <Action.h>
#include <Root.h>
/////////////////////////////////////////////////////////////////
class Command : public Action {
/////////////////////////////////////////////////////////////////


    public:
        Root* os;
        String text;
       Command(TreeNode* parent,String name);
       ~Command();
       virtual void onSetup(void);
       virtual void onCall(void);
};
/////////////////////////////////////////////////////////////////
class Query : public Command {
/////////////////////////////////////////////////////////////////
//internal call

    public:
        
        String text;
       Query(TreeNode* parent,String name,String text);
       ~Query();
       virtual void onCall(void);
};
/////////////////////////////////////////////////////////////////
class Execute : public Command {
/////////////////////////////////////////////////////////////////
//fuction call

    public:
        
       void (*function)();
       Execute(TreeNode* parent,String name,void (*function)() );
       ~Execute();
       virtual void onCall(void);
};
/////////////////////////////////////////////////////////////////
class Sleep : public Command {
/////////////////////////////////////////////////////////////////
//fuction call

    public:
        float dt=0.0;

       Sleep(TreeNode* parent,String name,float dt );
       ~Sleep();
       virtual void onCall(void);
};
/////////////////////////////////////////////////////////////////



#endif
