#ifndef ROOT_H
#define ROOT_H

#include <TreeNode.h>
#include <Device.h>
#include <Module.h>
#include <Action.h>
#include <ArduinoJson.h>


/////////////////////////////////////////////////////////////////
class Root : public Action {
/////////////////////////////////////////////////////////////////
public:
    bool debug=true;
    //---------------------------------------------------
    Root(String name): Action(name){
    //---------------------------------------------------

    };
   ~Root(){};
    //---------------------------------------------------
   virtual void onSetup(void){
    //---------------------------------------------------
        title("setup");
    };
    //---------------------------------------------------
   virtual void onCall(void){
    //---------------------------------------------------
        title("call");
        DynamicJsonDocument execution_frame(400);

        //StaticJsonDocument<400> execution_frame;
        JsonObject data=execution_frame.to<JsonObject>();

        arx::vector <Device *> devices=this->getNodes<Device>();
        arx::vector <Module *> modules=this->getChildren<Module>();

        for (unsigned i=0; i < devices.size(); i++) {
            Serial.println(devices[i]->name);
            devices[i]->read();
            //devices[i]->get_data(data);
            //Serial.println(execution_frame.memoryUsage()/execution_frame.capacity());
        }
/*
        for (unsigned i=0; i < modules.size(); i++) {
            modules[i]->call(data);
        }

        for (unsigned i=0; i < devices.size(); i++) {
            devices[i]->set_data(data);
            devices[i]->write();
        }

        if (this->debug){
            serializeJsonPretty(execution_frame, Serial);
        }
*/

    };

    //---------------------------------------------------
        virtual void onExec(void){
    //---------------------------------------------------


            //for (unsigned i=0; i < this->modules.size(); i++) {
            //    this->modules[i]->call();
            //}
        };

};
/////////////////////////////////////////////////////////////////


#endif
