#ifndef CARD_H
#define CARD_H

#include <TreeNode.h>
#include <ArduinoJson.h>
/////////////////////////////////////////////////////////////////
class Device : public TreeNode {
/////////////////////////////////////////////////////////////////

    /* 
    simple tree structure for arduino
    base for programs
    */ 

    public:

        bool is_input=true;
        bool is_output=true;

        Device(TreeNode* parent,String name):TreeNode(parent,name){};
        ~Device(){};

        virtual void onSetup(void){};

        void read(void){
            if (this->active and this->is_input){this->onRead();}   
        };

        void write(void){
            if (this->active and this->is_output){this->onWrite();}     
        };

       virtual void onRead(void){};
       virtual void onWrite(void){};
       virtual void get_data(JsonObject data){};
       virtual void set_data(JsonObject data){};
};
/////////////////////////////////////////////////////////////////
class TestDevice : public Device {
/////////////////////////////////////////////////////////////////
    public:

        TestDevice(TreeNode* parent,String name):Device(parent,name){}; 
        ~TestDevice(){};
        virtual void onSetup(void){this->active=true;this->log("setup");};
        virtual void onRead(void){this->log("read");};
        virtual void onWrite(void){this->log("write");};

};
/////////////////////////////////////////////////////////////////
class DeviceManager : public Device {
/////////////////////////////////////////////////////////////////

    public:
        arx::vector <Device *> devices;

        DeviceManager(TreeNode* parent,String name):Device(parent,name){}; 
        ~DeviceManager(){};

        virtual void onSetup(void){
            this->devices=this->getNodes<Device>();
        }; 

        virtual void onRead(void){

            for (unsigned i=0; i < this->devices.size(); i++) {
                this->devices[i]->read();
            }
        }; 

        virtual void onWrite(void){
            for (unsigned i=0; i < this->devices.size(); i++) {
                this->devices[i]->write();
            }
        }; 
};

/////////////////////////////////////////////////////////////////
#endif

