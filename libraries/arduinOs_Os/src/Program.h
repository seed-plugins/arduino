#ifndef PROGRAM_H
#define PROGRAM_H

#include <Action.h>
#include <ArduinoJson.h>

/////////////////////////////////////////////////////////////////
class Program : public Action {
/////////////////////////////////////////////////////////////////


    public:
       Program(TreeNode* parent,String name);
       ~Program();

       virtual void onCall(void);
       virtual void query(String command);
       //virtual void onQuery(JSONVar data);
       //JSONVar parse_data(String command);
};
/////////////////////////////////////////////////////////////////


#endif
