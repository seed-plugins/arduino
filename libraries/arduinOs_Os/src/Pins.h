#ifndef PINS_H
#define PINS_H

#include <TreeNode.h>
#include <Device.h>
#include <ArduinoJson.h>
//arx::map <int,int> pins_map;

//class Tone
// https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/
// https://www.arduino.cc/reference/en/language/functions/advanced-io/notone/

//void allocate_pin(int pin,int status){
//    pins_map[pin]=status;
//};

//TODO: analogic :  it will map input voltages between 0 and 5 volts 
// into integer values between 0 and 1023.
/////////////////////////////////////////////////////////////////
class SinglePin : public Device {
/////////////////////////////////////////////////////////////////

    public:
       int pin;
       int state;

       //----------------------------------------------------------------

       SinglePin(TreeNode* parent,String name,int pin,int state): Device(parent,name){
    
            this->pin=pin;
            pinMode(pin, state);
            //allocate_pin(pin,state);
        };
       //----------------------------------------------------------------
       ~SinglePin(){};
       //----------------------------------------------------------------
       virtual void get_data(JsonObject data){

            if (this->active and this->is_input){
                data[this->node_id]=this->state;
            }     
        };
       //----------------------------------------------------------------
       virtual void set_data(JsonObject data){

            auto value=data[this->node_id];
            if (this->active and this->is_output and value){
                this->state=value;
            }else{
                data[this->node_id]=this->state;
            }     
        };
       //----------------------------------------------------------------

};
/////////////////////////////////////////////////////////////////
class DigitalInput : public SinglePin {
/////////////////////////////////////////////////////////////////

    public:

       DigitalInput(TreeNode* parent,String name,int pin);
       ~DigitalInput(){};

       virtual void onRead(void);

};
/////////////////////////////////////////////////////////////////
class DigitalOutput : public SinglePin {
/////////////////////////////////////////////////////////////////

    public:
       DigitalOutput(TreeNode* parent,String name,int pin,bool state);
       ~DigitalOutput(){};

       virtual void onWrite(void);

       void set_on(void);
       void set_off(void);
};
/////////////////////////////////////////////////////////////////
class Blink : public DigitalOutput {
/////////////////////////////////////////////////////////////////

    public:
       Blink(TreeNode* parent,String name):DigitalOutput(parent,name,LED_BUILTIN,LOW){};
       ~Blink(){};

};

/////////////////////////////////////////////////////////////////
class AnalogicInput : public SinglePin {
/////////////////////////////////////////////////////////////////

    public:

       AnalogicInput(TreeNode* parent,String name,int pin);
       ~AnalogicInput(){};

       virtual void onRead(void);

};

/////////////////////////////////////////////////////////////////
class AnalogicOutput : public SinglePin {
/////////////////////////////////////////////////////////////////

    public:
       AnalogicOutput(TreeNode* parent,String name,int pin);
       ~AnalogicOutput(){};

       virtual void onWrite(void);

       void set(int value);
};

/////////////////////////////////////////////////////////////////



#endif
