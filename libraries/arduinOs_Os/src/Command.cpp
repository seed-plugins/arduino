#include "Command.h"


 
//----------------------------------------------------------------
Command::Command(TreeNode* parent,String name) : Action(parent,name){


}
//----------------------------------------------------------------
Command::~Command(){/*nothing to destruct*/}
//----------------------------------------------------------------
void Command::onSetup(void){

        this->os=this->root<Root>();

}
//----------------------------------------------------------------
void Command::onCall(void){

}
//----------------------------------------------------------------
Query::Query(TreeNode* parent,String name,String text) : Command(parent,name){
this->text=text;

}
//----------------------------------------------------------------
Query::~Query(){/*nothing to destruct*/}

//----------------------------------------------------------------
void Query::onCall(void){

        Action* node=this->root()->find<Action>(this->text);
        node->call();

}
//----------------------------------------------------------------
Execute::Execute(TreeNode* parent,String name,void (*function)()) : Command(parent,name){
this->function=function;

}
//----------------------------------------------------------------
Execute::~Execute(){/*nothing to destruct*/}

//----------------------------------------------------------------
void Execute::onCall(void){

        this->function();

}
//----------------------------------------------------------------
Sleep::Sleep(TreeNode* parent,String name,float dt) : Command(parent,name){
this->dt=dt;

}
//----------------------------------------------------------------
Sleep::~Sleep(){/*nothing to destruct*/}

//----------------------------------------------------------------
void Sleep::onCall(void){

        delay(this->dt*1000);

}
//----------------------------------------------------------------


//----------------------------------------------------------------

