
#ifndef USBSERIAL_H
#define USBSERIAL_H

#include <Device.h>

/////////////////////////////////////////////////////////////////
class StdSerial : public Device {
/////////////////////////////////////////////////////////////////


    public:

       StdSerial(TreeNode* parent,String name): Device(parent,name){};
       ~StdSerial();

        virtual void onSetup(void);
        virtual void onRead(void);

};
/////////////////////////////////////////////////////////////////


#endif
