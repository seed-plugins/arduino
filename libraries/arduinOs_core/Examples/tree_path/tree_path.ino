
#include <TreeNode.h>

///////////////////////////////////////////////////////
// construire un arbre d'éléments
///////////////////////////////////////////////////////
TestTree  root("main");

TestTree a(&root,"a");
TestTree b(&root,"b");
TestTree c(&root,"c");

TestTree aa(&a,"aa");
TestTree ab(&a,"ab");
TestTree ac(&a,"ac");


TestTree ba(&b,"ba");
TestTree bb(&b,"bb");
TestTree bc(&b,"bc");

TreeNode* r;
///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////
  //initialiser le port série
  Serial.begin(9600);
 while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  //afficher la structure
  title("setup");
  root.setup();

  title("tree");
  root.tree();

  title("search");
  r=root.find("");
  r->log("found empty");
line();
  r=root.find("/");
  r->log("found root");
line();
  r=root.find("b/b");
  r->log("found b/b");
line();
  r=root.find("a");
  r->log("found a");
line();
  r=root.find("/a");
  r->log("found /a");
line();
  r=root.find("/a/");
  r->log("found /a/");
line();
}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////

  //vide

}
///////////////////////////////////////////////////////
