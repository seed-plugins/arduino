
#include <TreeNode.h>

///////////////////////////////////////////////////////
// construire un arbre d'éléments
///////////////////////////////////////////////////////

TestTree root("main");

TestTree a(&root,"a");
TestTree b(&root,"b");
TestTree c(&root,"c");

TestTree aa(&a,"d");
TestTree ab(&a,"e");
TestTree ac(&a,"f");


TestTree ba(&b,"g");
TestTree bb(&b,"h");
TestTree bc(&b,"i");

///////////////////////////////////////////////////////
void setup() {
///////////////////////////////////////////////////////
    //initialiser le port série
    Serial.begin(9600);
    //afficher la structure
    //Serial.println("setup");
    //root.setup();

    Serial.println("recursive b off");
    b.off();

    arx::vector <TestTree *> nodes=root.getNodes<TestTree>();

    for (unsigned i=0; i < nodes.size(); i++) {
            Serial.println(nodes[i]->path());
        }
    a.off();
    b.on();
    Serial.println("recursive a off");

    nodes=root.getNodes<TestTree>();

    for (unsigned i=0; i < nodes.size(); i++) {
        Serial.println(nodes[i]->path());
    }

}
///////////////////////////////////////////////////////
void loop() {
///////////////////////////////////////////////////////

  //vide


}
///////////////////////////////////////////////////////
