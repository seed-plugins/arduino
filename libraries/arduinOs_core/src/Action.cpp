#include "Action.h" //include the declaration for this class
//////////////////////////////////////////////////////////////////

// Action

/////////////////////////////////////////////////////////////////


//----------------------------------------------------------------     
float Action::now(void){
//seconds
        int t=millis();
        return float(t)/1000.0;

}

//----------------------------------------------------------------     
void Action::call(void){
    //Serial.println(this->active);
    if (this->active){
        this->onCall();
        this->callChildren();
    }
}
//----------------------------------------------------------------
void Action::callChildren(void){

    arx::vector <Action *> actions=this->getChildren<Action>();
    for (unsigned i=0; i < actions.size(); i++) {
        actions[i]->call();
    }
}
//----------------------------------------------------------------





//////////////////////////////////////////////////////////////////

