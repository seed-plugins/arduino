#include "tools.h"
/////////////////////////////////////////////////////////////////    

String string_multiply(int n,String str){

/////////////////////////////////////////////////////////////////

    String result="";

    for (unsigned i=0; i <n; i++) {
        result+=str;
    }
    return result;

};
/////////////////////////////////////////////////////////////////    

String string_merge(arx::vector<String> lst){

/////////////////////////////////////////////////////////////////

    String result="";

    for (unsigned i=0; i <lst.size(); i++) {
        result+=lst[i];
    }
    return result;

};
/////////////////////////////////////////////////////////////////    

arx::vector<String> split_string(String path,String separator){

/////////////////////////////////////////////////////////////////

    String string=String();
    arx::vector <String> result;

    for(unsigned int i = 0; i<path.length(); i++) {

        if (String(path[i]) ==separator ){
            if(string!=""){
                result.push_back(string);
            }
            string=String();
        }else{
            string+=path[i];
        }

    }
    if(string!=""){
        result.push_back(string);
    }
    return result;
};


/////////////////////////////////////////////////////////////////

void title(String str){

/////////////////////////////////////////////////////////////////

    line();
    Serial.print("      ") ;
    Serial.println(str) ;
    line();
}
/////////////////////////////////////////////////////////////////

void line(void){

/////////////////////////////////////////////////////////////////

       Serial.println("############################################") ;
}

/////////////////////////////////////////////////////////////////


