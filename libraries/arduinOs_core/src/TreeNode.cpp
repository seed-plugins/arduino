#include "TreeNode.h" //include the declaration for this class
// Create/destroy
//------------------------------------------------
TreeNode::TreeNode(){
//------------------------------------------------
    this->name="root";
} 
//root creation, no parent
//------------------------------------------------
TreeNode::TreeNode(String name){
//------------------------------------------------
    this->name=name;
}   

//children creation, parent reference  
//------------------------------------------------           
TreeNode::TreeNode(TreeNode* parent,String name){
//------------------------------------------------
    this->name=name;
    this->parent=parent;
    this->position=this->parent->children.size();
    this->parent->children.push_back(this);
    this->node_id=this->path();
}
//------------------------------------------------
    TreeNode::~TreeNode(){
//------------------------------------------------
    this->parent->children.erase(this->parent->children.begin()+this->position);
    this->parent=NULL;
    arx::vector <TreeNode *> nodes=this->getChildren<TreeNode>();
    for (unsigned i=0; i < nodes.size(); i++) {
        delete nodes[i];
    }
};  
//----------------------------------------------------------------     
void TreeNode::setup(void){

    this->log("setup");
    this->onSetup();
    arx::vector <TreeNode *> nodes=this->getChildren<TreeNode>();
    for (unsigned i=0; i < nodes.size(); i++) {
        nodes[i]->setup();
    }
}
//----------------------------------------------------------------
TreeNode* TreeNode::root(){
  
    if(this->parent){return this->parent->root();}
    else{return this;}
}
//----------------------------------------------------------------
String TreeNode::path(void){
    //Serial.println(this->name);
    if ( this-> parent and  this-> parent-> parent  ){
        return this->parent->path()+"/"+this->name;

    }else if ( this-> parent ){
        return "/"+this->name;

    }else{
        return "/";
    }

}
//----------------------------------------------------------------     
void TreeNode::tree(void){
    this->tree(1);
}
//----------------------------------------------------------------     
void TreeNode::tree(int level){

    Serial.print(string_multiply(level,"   |"));
    Serial.print(" * ");
    Serial.println(this->name);
    Serial.flush();

    for (unsigned i=0; i < this->children.size(); i++) {
        this->children[i]->tree(level+1);
    }
}
//----------------------------------------------------------------
TreeNode* TreeNode::find(String path){

    TreeNode* node;


    if(path[0]=='/'){
        //Serial.println("root");
        node=this->root();
    }else{
        //Serial.println("local");
        node=this;
    }

    arx::vector<String> list=split_string(path,String("/") );

    for (unsigned i=0; i < list.size(); i++) {
        //Serial.println(list[i]);
        if (list[i].length() >0) {
            node=node->find_child(list[i]);

            if(!node){
                return NULL;
            }
        }else if (list[i]=="."){

        }else if (list[i]==".."){
            node=node->parent;
        }


    }
    return node;
}
//----------------------------------------------------------------
TreeNode* TreeNode::find_child(String name){

    for (unsigned i=0; i < this->children.size(); i++) {
        if (this->children[i]->name == name ){
            return this->children[i];
        }
    }
    return NULL;
}
//----------------------------------------------------------------
void TreeNode::log(String message){ 
//----------------------------------------------------------------
    Serial.println(this->node_id+" -> "+message);

}/*
//----------------------------------------------------------------
void Messages::log_clear(void){ 
//----------------------------------------------------------------
    this->messages.clear();
    for (unsigned i=0; i < this->children.size(); i++) {
        this->children[i]->log_clear();
    }  
}
//----------------------------------------------------------------     
String Messages::print_messages(void){

    String full_msg="";

    for (unsigned i=0; i < this->messages.size(); i++) {
        Serial.println(this->messages[i]);
        full_msg=full_msg+String("MSG :");
        full_msg=full_msg+String(this->messages[i]);
        full_msg=full_msg+String("\n");
    }
    return full_msg;
    /*for (unsigned j=0; j < this->children.size(); j++) {
        this->children[j]->print_messages(level+1);
    }*/
//}
//----------------------------------------------------------------






//////////////////////////////////////////////////////////////////

