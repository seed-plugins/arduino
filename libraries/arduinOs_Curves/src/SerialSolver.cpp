#include "SerialSolver.h"



//----------------------------------------------------------------
void SerialSolver::onSetup(void){
    this->nodes=this->getChildren<Curve>();
}
//----------------------------------------------------------------
void SerialSolver::onCall(void){

    float t=this->now();
    String result="";

    for (unsigned i=0; i < this->nodes.size(); i++) {
        result+=String(this->nodes[i]->solve(t))+",";
    }
    Serial.println( result ) ;

}
//----------------------------------------------------------------

