#ifndef CURVE_H
#define CURVE_H

#include <TreeNode.h>

/////////////////////////////////////////////////////////////////
class Curve : public TreeNode {
/////////////////////////////////////////////////////////////////


    public:

        float value=0.0;
        arx::vector <Curve *> curves;

       Curve(TreeNode* parent,String name);
       ~Curve();
       virtual void onSetup();
       virtual float solve(float t);

};

/////////////////////////////////////////////////////////////////


#endif
