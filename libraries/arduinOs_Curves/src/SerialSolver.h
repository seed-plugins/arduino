#ifndef SerialSolver_H
#define SerialSolver_H

#include <Action.h>
#include <Curve.h>
/////////////////////////////////////////////////////////////////
class SerialSolver : public Action {
/////////////////////////////////////////////////////////////////


    public:
    arx::vector <Curve *> nodes;
    // Create/destroy
    //------------------------------------------------
        SerialSolver():Action(){};   
        SerialSolver(String name):Action(name){};                  
        SerialSolver(TreeNode* parent,String name):Action(parent,name){}; 
        ~SerialSolver(){};                          //destroy, not done

       virtual void onSetup(void);
       virtual void onCall(void);

};

/////////////////////////////////////////////////////////////////


#endif
